package com.example.clicker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class kitchen extends AppCompatActivity {
    Button Up;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen);
        Up = (Button)findViewById(R.id.Up);
        Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.countMoney >= 500){
                    MainActivity.addMoney += 2;
                    MainActivity.countMoney -= 500;
                    MainActivity.textMoney.setText(MainActivity.countMoney + " ");
                }
            }
        });
    }
}
