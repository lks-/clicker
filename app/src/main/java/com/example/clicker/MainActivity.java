package com.example.clicker;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    static public int countMoney = 0;
    static public int addMoney = 10;
    Button btnClick, kitchen;
    static public TextView textMoney;
    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnClick = (Button)findViewById(R.id.btnClick);
        kitchen = (Button)findViewById(R.id.kitchen);
        textMoney = (TextView)findViewById(R.id.money);
        btnClick();
    }

    void btnClick(){
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countMoney += addMoney;
                textMoney.setText(countMoney + "");
            }
        });
        kitchen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, kitchen.class);
                startActivity(intent);
            }
        });
    }

}
